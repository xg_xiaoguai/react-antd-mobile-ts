# react-antd-mobile-ts
vite + react + antd-mobile移动端前端模板


#### 安装教程

1.  克隆项目
```
git clone https://gitee.com/xg_xiaoguai/react-antd-mobile-ts.git
```
2.  进入项目
```
cd react-antd-mobile-ts
```
3.  安装依赖
```
npm install
```
4.  运行
```
npm run dev
```
5.  打包
```
npm run build
```
6.  本地预览
```
// 需要先运行打包命令！本地启动一个服务器，访问打包后的dist目录，方便测试打包后是否存在问题
npm run preview
```

7、eslint校验
```
// 校验
npm run lint

// 修复
npm run lint -- --fix
```