import { BrowserRouter as Router, Routes, Route, useRoutes } from 'react-router-dom';
import { lazy } from 'react'
import App from './App'
import Test from './Test'
const TabBar = lazy(() => import('./views/tabBar/Index'))
const Meals = lazy(() => import('./views/meals/Index'))
// function TheTouter() {
//   console.log(123)
//   return (
//     <Router>
//       <Routes>
//         <Route key={1} path='/index' element={<TabBar />}></Route>
//         <Route key={2} path='/index/meals' element={<Meals />}></Route>
//         <Route key={3} path='test' element={<Test />}></Route>
//         <Route key={4} path="/app" element={<App />}></Route>
//       </Routes>
//     </Router>
//   )
// }

// export default TheTouter
export default () => {
  const routes = useRoutes([
    {
      path: '/index',
      element: <TabBar />,
      children: [
        {
          path: 'meals',
          element: <Meals />
        }
      ]
    },
    {
      path: 'test',
      element: <Test />
    },
    {
      path: 'app',
      element: <App />
    }
  ])
  console.log(routes)
  return (routes)
}