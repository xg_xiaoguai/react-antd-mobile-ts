import { TabBar } from "antd-mobile"
import { TruckOutline, TransportQRcodeOutline, FlagOutline, TeamOutline, CalendarOutline } from 'antd-mobile-icons'
import './tabbar.css'

export default () => {
  const tabs = [
    {
      title: '餐食',
      icon: <TruckOutline />
    },
    {
      title: '机供品',
      icon: <TransportQRcodeOutline />
    },
    {
      title: '任务',
      icon: <FlagOutline />
    },
    {
      title: '管理',
      icon: <TeamOutline />
    },
    {
      title: '值班',
      icon: <CalendarOutline />
    }
  ]
  return (
    <div className="body">
      <a href="/test" >title</a>
      <div className="main">
        <p style={{height: 2000}}>main</p>
      </div>
      <TabBar className="xg-tabbar">
        {
          tabs.map(item => {
            return <TabBar.Item key={item.title} title={item.title} icon={item.icon}></TabBar.Item>
          })
        }
      </TabBar>
    </div>
  )
}