import React from 'react'
import ReactDOM from 'react-dom/client'
import TheRouter from './router'
import RouterConfig from './router-config'
import { BrowserRouter as Router } from 'react-router-dom'
import './index.css'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Router>
    <React.StrictMode>
      <RouterConfig />
    </React.StrictMode>
  </Router>,
    // <React.StrictMode>
    //   <TheRouter />
    // </React.StrictMode>
)
