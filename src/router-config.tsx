import { BrowserRouter as Router, Routes, Route, useRoutes } from 'react-router-dom';
import { lazy } from 'react'
import App from './App'
import Test from './Test'
const TabBar = lazy(() => import('./views/tabBar/Index'))
const Meals = lazy(() => import('./views/meals/Index'))

export default () => {
  const routes = useRoutes([
    {
      path: '/index',
      element: <TabBar />,
      children: [
        {
          path: 'meals',
          element: <Meals />
        }
      ]
    },
    {
      path: 'test',
      element: <Test />
    },
    {
      path: 'app',
      element: <App />
    }
  ])
  return (routes)
}